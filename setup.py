#! /usr/bin/env python

"""TODO: Maybe add a docstring containing a long description for mesop?
"""

from setuptools import find_packages, setup
import os

import mesop

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(name='mesop',
      version=mesop.__version__,
      author='mesop',
      author_email='mesop',
      description='Energy hub modell',
      namespace_package=['mesop'],
      long_description=read('README.rst'),
      packages=find_packages(),
      package_data={
            'mesop': [os.path.join('tools', 'default_files', '*.ini')]},
      install_requires=['dill',
                        'numpy >= 1.7.0',
                        'pandas >= 0.18.0',
                        'pyomo >= 4.2.0, != 4.3.11377, <=5.3',
                        'matplotlib'],
     )
