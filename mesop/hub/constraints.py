# -*- coding: utf-8 -*-

"""Additional constraints to be used in an mesop energy model.
This file is part of project mesop (github.com/mesop/mesop). It's copyrighted
by the contributors recorded in the version control history of the file,
available from its original location mesop/mesop/hub/constraints.py

SPDX-License-Identifier: GPL-3.0-or-later
"""

import pyomo.environ as po


def investment_limit(model, limit=None):
    """ Set an absolute limit for the total investment costs of an investment
    optimization problem:

    .. math:: \sum_{investment\_costs} \leq limit

    Parameters
    ----------
    model : mesop.solph.Model
        Model to which the constraint is added
    limit : float
        Absolute limit of the investment (i.e. RHS of constraint)
    """

    def investment_rule(m):
        expr = 0

        if hasattr(m, "InvestmentFlow"):
            expr += m.InvestmentFlow.investment_costs

        return expr <= limit

    model.investment_limit = po.Constraint(rule=investment_rule)

    return model


def emission_limit(om, flows=None, limit=None):
    """Set a global limit for emissions. The emission attribute has to be added
    to every flow you want to take into account.

    Parameters
    ----------
    om : mesop.solph.Model
        Model to which constraints are added.
    flows : dict
        Dictionary holding the flows that should be considered in constraint.
        Keys are (source, target) objects of the Flow. If no dictionary is
        given all flows containing the 'emission' attribute will be used.
    limit : numeric
        Absolute emission limit.

    Note
    ----
    Flow objects required an emission attribute!

    """

    if flows is None:
        flows = {}
        for (i, o) in om.flows:
            if hasattr(om.flows[i, o], 'emission'):
                flows[(i, o)] = om.flows[i, o]

    else:
        for (i, o) in flows:
            if not hasattr(flows[i, o], 'emission'):
                raise ValueError(('Flow with source: {0} and target: {1} '
                                 'has no attribute emission.').format(i.label,
                                                                      o.label))

    def emission_rule(m):
        return (sum(m.flow[inflow, outflow, t] * m.timeincrement[t] *
                    flows[inflow, outflow].emission
                for (inflow, outflow) in flows
                for t in m.TIMESTEPS) <= limit)

    om.emission_limit = po.Constraint(rule=emission_rule)

    return om


